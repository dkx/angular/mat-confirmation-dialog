import {Component, Inject, ChangeDetectionStrategy} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';


export declare interface MatConfirmationDialogData
{
	message: string,
	trueButtonTitle: string,
	falseButtonTitle: string,
	title?: string,
}


@Component({
	templateUrl: './mat-confirmation-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatConfirmationDialogComponent
{


	constructor(
		@Inject(MAT_DIALOG_DATA) public readonly data: MatConfirmationDialogData,
	) {}

}
