import {InjectionToken} from '@angular/core';


export const MAT_CONFIRMATION_DIALOG_CONFIG = new InjectionToken('MAT_CONFIRMATION_DIALOG_CONFIG');


export declare interface MatConfirmationDialogConfig
{
	trueButtonTitle?: string,
	falseButtonTitle?: string,
}
