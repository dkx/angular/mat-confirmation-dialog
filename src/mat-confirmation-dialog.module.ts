import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule, MatButtonModule} from '@angular/material';
import {A11yModule} from '@angular/cdk/a11y';

import {MatConfirmationDialogComponent} from './mat-confirmation-dialog.component';
import {MatConfirmationDialogService} from './mat-confirmation-dialog.service';


@NgModule({
	imports: [
		CommonModule,
		A11yModule,
		MatDialogModule,
		MatButtonModule,
	],
	declarations: [
		MatConfirmationDialogComponent,
	],
	entryComponents: [
		MatConfirmationDialogComponent,
	],
	providers: [
		MatConfirmationDialogService,
	],
})
export class MatConfirmationDialogModule {}
