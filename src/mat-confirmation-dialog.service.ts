import {Inject, Injectable, Optional} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {MatConfirmationDialogComponent, MatConfirmationDialogData} from './mat-confirmation-dialog.component';
import {MAT_CONFIRMATION_DIALOG_CONFIG, MatConfirmationDialogConfig} from './mat-confirmation-dialog-config';


export declare interface MatConfirmationDialogOptions
{
	title?: string,
	trueButtonTitle?: string,
	falseButtonTitle?: string,
}


const DEFAULT_TRUE_BUTTON_TITLE: string = 'Yes';
const DEFAULT_FALSE_BUTTON_TITLE: string = 'No';


@Injectable()
export class MatConfirmationDialogService
{


	private readonly trueButtonTitle: string;

	private readonly falseButtonTitle: string;


	constructor(
		private $dialog: MatDialog,
		@Inject(MAT_CONFIRMATION_DIALOG_CONFIG) @Optional() config: MatConfirmationDialogConfig|null,
	) {
		if (config === null) {
			config = {};
		}

		this.trueButtonTitle = typeof config.trueButtonTitle === 'undefined' ? DEFAULT_TRUE_BUTTON_TITLE : config.trueButtonTitle;
		this.falseButtonTitle = typeof config.falseButtonTitle === 'undefined' ? DEFAULT_FALSE_BUTTON_TITLE : config.falseButtonTitle;
	}


	public open(message: string, options: MatConfirmationDialogOptions = {}): MatDialogRef<MatConfirmationDialogComponent, boolean>
	{
		const data: MatConfirmationDialogData = {
			message,
			trueButtonTitle: typeof options.trueButtonTitle === 'undefined' ? this.trueButtonTitle : options.trueButtonTitle,
			falseButtonTitle: typeof options.falseButtonTitle === 'undefined' ? this.falseButtonTitle : options.falseButtonTitle,
		};

		if (typeof options.title !== 'undefined') {
			data.title = options.title;
		}

		return this.$dialog.open(MatConfirmationDialogComponent, {
			data,
		});
	}

}
