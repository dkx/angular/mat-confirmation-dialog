# DKX/Angular/MatConfirmationDialog

Confirmation dialog for angular material

## Installation

```bash
$ npm install --save @dkx/mat-confirmation-dialog
```

or with yarn

```bash
$ yarn add @dkx/mat-confirmation-dialog
```

## Usage

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {MatConfirmationDialogModule} from '@dkx/mat-confirmation-dialog';

@NgModule({
    imports: [
        MatConfirmationDialogModule,
    ],
})
export class AppModule {}
```

**Component:**

```typescript
import {Component} from '@angular/core';
import {MatConfirmationDialogService} from '@dkx/mat-confirmation-dialog';

@Component({...})
export class MyComponent
{
    
    constructor(
        private confirmDialog: MatConfirmationDialogService,
    ) {}
    
    public openConfirmDialog(): void
    {
        const cd = this.confirmDialog.open('Are you sure?');
        
        cd.afterClosed().subscribe((result: boolean) => {
            if (result) {
                alert('Clicked on "yes"');
            } else {
                alert('Clicked on "no"');
            }
        });
    }
    
}
```

## Configuration

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {MatConfirmationDialogModule, MatConfirmationDialogConfig, MAT_CONFIRMATION_DIALOG_CONFIG} from '@dkx/mat-confirmation-dialog';

const confirmDialogConfig: MatConfirmationDialogConfig = {
    trueButtonTitle: 'Maybe yes',
    falseButtonTitle: 'Maybe no',
};

@NgModule({
    imports: [
        MatConfirmationDialogModule,
    ],
    providers: [
        {
            provide: MAT_CONFIRMATION_DIALOG_CONFIG,
            useValue: confirmDialogConfig,
        },
    ],
})
export class AppModule {}
```
